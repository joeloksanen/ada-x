from anytree import PostOrderIter, PreOrderIter
from functools import reduce
from agent.SA.bert_analyzer import BertAnalyzer
from agent.target_extraction.product import Product
from agent.review import Review
from agent.dataloader import DataLoader
from agent.argument import Argument
import pickle
import re
from time import time


class Framework:
    HIGH_SENTIMENT_THRESHOLD = 0.99
    bert_analyzer = BertAnalyzer.load_saved('agent/SA/semeval_2014_both_unmasked_6.pt')

    def __init__(self, product_type, product_id):
        self.product_id = product_id
        self.product = Product.get_product(product_type)
        self.product_node = self.product.root
        self.arguments = self.product.argument_nodes
        self.features = self.product.feature_nodes

        ts = time()

        # get reviews
        review_csv = DataLoader.get_reviews(product_id)
        reviews = [Review(row, self.product) for _, row in review_csv.head(1000).iterrows()]

        t_feature = time()
        print('Feature detection took {} seconds'.format(t_feature - ts))

        # extract augmented votes
        self.extract_votes(reviews)
        voting_reviews = list(filter(lambda r: r.is_voting(), reviews))
        if len(voting_reviews) / len(reviews) < 0.33:
            print('warning: only a small fraction of reviews generated votes')

        t_sa = time()
        print('Sentiment analysis took {} seconds'.format(t_sa - t_feature))

        # get aggregates
        ra, self.vote_sum, self.vote_phrases = self.get_aggregates(reviews)

        t_ra = time()
        print('Review aggregation took {} seconds'.format(t_ra - t_sa))

        # get qbaf from ra
        self.qbaf, self.argument_polarities = self.get_qbaf(ra, len(reviews))

        # apply gradual semantics
        self.strengths = self.get_strengths(self.qbaf)

        te = time()
        print('QBAF construction took {} seconds'.format(te - t_ra))
        print('Process took {} seconds'.format(te - ts))

        # save
        self.save()

        # print results
        self.print()

    def print(self):
        print('qbaf:')
        print(self.qbaf)
        for argument in self.arguments:
            print(argument.name)
            print('  strength:', self.strengths[argument])
            print('  polarity:', 'positive' if self.argument_polarities[argument] else 'negative')
            print('  votes:')
            print('    direct: {} positive, {} negative'.format(len(self.supporting_phrases(argument)),
                                                                len(self.attacking_phrases(argument))))
            print('    augmented sum: {}'.format(self.vote_sum[argument]))

    def get_bert_sentiments(self, data):
        return list(self.bert_analyzer.get_batch_sentiment_polarity(data))

    def extract_votes(self, reviews):
        labelled_phrases = [(phrase.text, arg.start, arg.end) for review in reviews for phrase in review.phrases for arg
                            in phrase.args]

        sentiments = self.get_bert_sentiments(labelled_phrases)

        for review in reviews:
            for phrase in review.phrases:
                for arg in phrase.args:
                    sentiment = sentiments.pop(0)
                    arg.set_sentiment(sentiment)

    def get_aggregates(self, reviews):
        ra = []
        vote_sum = {arg: 0 for arg in self.arguments}
        vote_phrases = {arg: [] for arg in self.arguments}
        for review in reviews:
            for phrase in review.phrases:
                for arg, sentiment in phrase.get_votes().items():
                    vote_phrases[arg].append(phrase)  # {'phrase': phrase.text, 'sentiment': sentiment, 'n_args': len(phrase.args)}
            for arg, sentiment in review.get_votes().items():
                ra.append({'review_id': review.id, 'argument': arg, 'vote': sentiment})
                vote_sum[arg] += sentiment
        return ra, vote_sum, vote_phrases

    def get_qbaf(self, ra, review_count):
        # sums of all positive and negative votes for arguments
        argument_sums = {}
        for argument in self.arguments:
            argument_sums[argument] = 0
            for r in ra:
                if r['argument'] == argument:
                    argument_sums[argument] += r['vote']

        # calculate attack/support relations
        argument_polarities = {}
        supporters = {r: [] for r in self.arguments}
        attackers = {r: [] for r in self.arguments}
        for r in self.arguments:
            argument_polarities[r] = argument_sums[r] > 0
            for subf in r.children:
                if (argument_sums[r] > 0 and argument_sums[subf] > 0) or (argument_sums[r] < 0 and argument_sums[subf] < 0):
                    supporters[r].append(subf)
                elif (argument_sums[r] > 0 and argument_sums[subf] < 0) or (argument_sums[r] < 0 and argument_sums[subf] > 0):
                    attackers[r].append(subf)

        # calculate base scores for arguments
        base_strengths = {self.product_node: 0.5 + 0.5 * argument_sums[self.product_node] / review_count}
        for feature in self.features:
            base_strengths[feature] = abs(argument_sums[feature]) / review_count

        qbaf = {'supporters': supporters, 'attackers': attackers,
                'base_strengths': base_strengths}
        return qbaf, argument_polarities

    @staticmethod
    def combined_strength(args):
        if len(args) != 0:
            return 1 - reduce(lambda x, y: x * y, map(lambda v: 1 - v, args))
        return 0

    @staticmethod
    def argument_strength(base_score, attacker_strengths, supporter_strengths):
        attack = Framework.combined_strength(attacker_strengths)
        support = Framework.combined_strength(supporter_strengths)
        if attack > support:
            return base_score - (base_score * abs(attack - support))
        elif attack < support:
            return base_score + ((1 - base_score) * abs(attack - support))
        return base_score

    # apply DF-QUAD gradual semantics to qbaf
    # CHANGES TO INTERIM REPORT METHOD
    def get_strengths(self, qbaf):
        strengths = {}
        arguments = [node for node in PostOrderIter(self.product_node)]
        for argument in arguments:
            attacker_strengths = []
            supporter_strengths = []
            for child in argument.children:
                if child in qbaf['attackers'][argument]:
                    attacker_strengths.append(strengths[child])
                elif child in qbaf['supporters'][argument]:
                    supporter_strengths.append(strengths[child])
            strengths[argument] = Framework.argument_strength(qbaf['base_strengths'][argument], attacker_strengths,
                                                              supporter_strengths)
        return strengths

    def save(self):
        f = open('agent/extracted_frameworks/{}.pickle'.format(self.product_id), 'wb')
        pickle.dump(self, f)
        f.close()

    @staticmethod
    def load_saved(product_id):
        f = open('agent/extracted_frameworks/{}.pickle'.format(product_id), 'rb')
        agent = pickle.load(f)
        f.close()
        return agent

    def get_strongest_supporting_subfeature(self, argument):
        supporters = self.qbaf['supporters'][argument]
        if len(supporters) == 0:
            return None
        supporter_strengths = {s: self.strengths[s] for s in supporters}
        return max(supporter_strengths, key=supporter_strengths.get)

    def get_strongest_attacking_subfeature(self, argument):
        attackers = self.qbaf['attackers'][argument]
        if len(attackers) == 0:
            return None
        attacker_strengths = {a: self.strengths[a] for a in attackers}
        return max(attacker_strengths, key=attacker_strengths.get)

    def liked_argument(self, argument):
        return self.argument_polarities[argument]
        # self.vote_sum[argument] >= 0
        # len(self.supporting_phrases(argument)) >= len(self.attacking_phrases(argument))

    def supported_argument(self, argument):
        supp = self.get_strongest_supporting_subfeature(argument)
        return supp is not None and self.strengths[supp] > 0

    def attacked_argument(self, argument):
        att = self.get_strongest_attacking_subfeature(argument)
        return att is not None and self.strengths[att] > 0

    def best_supporting_phrase(self, argument):
        phrases = [phrase for phrase in self.supporting_phrases(argument)
                   if phrase.n_args() == 1 and Framework.is_well_formatted(phrase.text)]
        if len(phrases) == 0:
            return None
        top_5 = list(sorted(phrases, key=lambda p: p.get_vote(argument), reverse=True))[:5]
        return max(top_5, key=lambda p: len(p.text))

    def best_attacking_phrase(self, argument):
        phrases = [phrase for phrase in self.attacking_phrases(argument)
                   if phrase.n_args() == 1 and Framework.is_well_formatted(phrase.text)]
        if len(phrases) == 0:
            return None
        top_5 = list(sorted(phrases, key=lambda p: p.get_vote(argument)))[:5]
        return max(top_5, key=lambda p: len(p.text))

    @staticmethod
    def is_well_formatted(phrase):
        return re.match('^[-a-zA-Z0-9();,./!?\'" ]*$', phrase)

    def supporting_phrases(self, argument):
        return list(filter(lambda phrase: phrase.get_vote(argument) > 0, self.vote_phrases[argument]))

    def attacking_phrases(self, argument):
        return list(filter(lambda phrase: phrase.get_vote(argument) < 0, self.vote_phrases[argument]))

    def get_argument_graph(self):
        return self.create_arg(self.product_node, 120)

    def create_arg(self, arg_node, size):
        supporters = [self.create_arg(supp_node, size - 20) for supp_node in self.qbaf['supporters'][arg_node]]
        attackers = [self.create_arg(att_node, size - 20) for att_node in self.qbaf['attackers'][arg_node]]
        phrase = self.best_supporting_phrase(arg_node) if self.argument_polarities[arg_node] else self.best_attacking_phrase(arg_node)
        return Argument(arg_node.name, self.argument_polarities[arg_node], supporters, attackers, phrase, size)
