class ArgumentQuery:

    def __init__(self, queryID, text):
        self.queryID = queryID
        self.text = text

    def with_argument(self, argument):
        query = ArgumentQuery(self.queryID, self.text)
        query.argumentID = argument.id
        query.text = query.text.format(arg=argument.name)
        return query
