import pandas as pd


class DataLoader:
    data_location = 'agent/amazon_data/reviews.tsv'
    reviews = pd.read_csv(data_location, sep='\t', error_bad_lines=False)

    @staticmethod
    def get_reviews(product_id):
        return DataLoader.reviews[DataLoader.reviews['product_id'] == product_id].reset_index(drop=True)

    @staticmethod
    def get_product_name(product_id):
        return DataLoader.get_reviews(product_id)['product_title'][0]

    @staticmethod
    def get_avg_star_rating(product_id):
        return float(DataLoader.get_reviews(product_id)['star_rating'].mean())
