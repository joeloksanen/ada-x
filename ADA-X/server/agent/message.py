class ADAMessage:

    def __init__(self, text, arguments):
        self.text = text
        self.arguments = arguments


class ADAText:

    def __init__(self, template, arguments, style='ARG'):
        self.template = template
        self.arguments = arguments
        self.style = style

    def add(self, template_add, arguments_add=None):
        if arguments_add is None:
            arguments_add = []
        self.template += template_add
        self.arguments += arguments_add
