import re
from nltk.tokenize import TreebankWordTokenizer, sent_tokenize
from agent.SA.bert_dataset import MAX_SEQ_LEN
from anytree import PostOrderIter
from nltk.stem import WordNetLemmatizer

wnl = WordNetLemmatizer()
tokenizer = TreebankWordTokenizer()


class Review:
    SENTIMENT_THRESHOLD = 0.95
    PHRASE_MAX_WORDS = MAX_SEQ_LEN * 0.3

    def __init__(self, data, product):
        self.product = product
        self.id = data['review_id']
        self.body = data['review_body']
        self.phrases = Review.extract_phrases(self.body, product)
        self.votes = {}

    # extract phrases
    @staticmethod
    def extract_phrases(review_body, product):
        sentences = sent_tokenize(review_body)
        texts = []
        for sentence in sentences:
            texts += re.split(' but | although | though | otherwise | however | unless | whereas | despite |<br />',
                              sentence)
        texts = filter(lambda t: len(t.split()) < Review.PHRASE_MAX_WORDS, texts)
        phrases = [Phrase(text, product) for text in texts]
        return phrases

    def get_votes(self):
        for arg, sentiment in [(arg, sentiment) for phrase in self.phrases for arg, sentiment in phrase.votes.items()]:
            if arg not in self.votes or abs(sentiment) > abs(self.votes[arg]):
                self.votes[arg] = sentiment
        # normalize
        for arg in self.votes:
            self.votes[arg] = 1 if self.votes[arg] > 0 else -1
        self.augment_votes()
        return self.votes

    # augment votes (Definition 4.3) obtained for a single critic
    def augment_votes(self):
        arguments = [node for node in PostOrderIter(self.product.root)]
        for argument in arguments:
            if argument not in self.votes:
                polar_sum = 0
                for subfeat in argument.children:
                    if subfeat in self.votes:
                        polar_sum += self.votes[subfeat]
                if polar_sum != 0:
                    self.votes[argument] = 1 if polar_sum > 0 else -1

    def is_voting(self):
        return len(self.votes) > 0


class Phrase:

    def __init__(self, text, product):
        self.product = product
        self.text = text
        self.spans = list(tokenizer.span_tokenize(text))
        self.tokens = [text[start:end] for start, end in self.spans]
        self.args = self.get_args()
        self.votes = {}

    # get argument(s) that match phrase
    def get_args(self):
        argument_matches = []
        arguments = [node for node in PostOrderIter(self.product.root)]
        while len(arguments) > 0:
            arg = arguments.pop(0)
            for term in self.product.glossary[arg]:
                matches = [Arg(arg, ' '.join(term), start, end)
                           for start, end in Phrase.matching_subsequences(term, self.tokens)]
                if matches:
                    argument_matches += matches
                    self.remove_ancestors(arg, arguments)
                    break
        return argument_matches

    # remove all ancestors of node in list l
    def remove_ancestors(self, node, l):
        if node.parent is not None:
            try:
                l.remove(node.parent)
            except ValueError:
                pass
            self.remove_ancestors(node.parent, l)

    # def add_arg(self, arg):
    #     self.args.append(arg)

    def num_args(self):
        return len(self.args)

    def get_votes(self):
        for arg in self.args:
            if (abs(arg.sentiment) > Review.SENTIMENT_THRESHOLD and
                    (arg.node not in self.votes or abs(arg.sentiment) > abs(self.votes[arg.node]))):
                self.votes[arg.node] = arg.sentiment
        return self.votes

    def get_vote(self, node):
        return self.votes[node]

    def get_arg_mentions(self, node):
        mentions = []
        for arg in self.args:
            if arg.node == node:
                start, end = self.spans[arg.start][0], self.spans[arg.end - 1][1]
                mentions.append((arg.form, start, end))
        return mentions

    def n_args(self):
        return len(self.args)

    @staticmethod
    def matching_subsequences(l_sub, l):
        sub_idxs = []
        len_sub = len(l_sub)
        for i in range(len(l)):
            if l[i:i+len_sub] == l_sub:
                sub_idxs.append((i, i+len_sub))
        return sub_idxs


class Arg:

    def __init__(self, node, form, start, end):
        self.node = node
        self.form = form
        self.start = start
        self.end = end
        self.sentiment = None

    def set_sentiment(self, sentiment):
        self.sentiment = sentiment
