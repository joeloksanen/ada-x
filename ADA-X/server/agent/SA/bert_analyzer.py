import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from agent.SA.tdbertnet import TDBertNet
from agent.SA.bert_dataset import BertDataset, Instance, polarity_indices, generate_batch
import time
import numpy as np
from sklearn import metrics

device = torch.device('cuda')

semeval_2014_test_path = 'data/SemEval-2014/Laptops_Test_Gold.xml'
amazon_test_path = 'data/Amazon/annotated_amazon_laptop_reviews.xml'

BATCH_SIZE = 32
MAX_EPOCHS = 6
LEARNING_RATE = 0.00002
loss_criterion = nn.CrossEntropyLoss()


def loss(outputs, labels):
    return loss_criterion(outputs, labels)


class BertAnalyzer:

    # @staticmethod
    # def default():
    #     sa = BertAnalyzer()
    #     sa.load_saved(trained_model_path)
    #     return sa

    @staticmethod
    def load_saved(path):
        sa = BertAnalyzer()
        sa.net = TDBertNet(len(polarity_indices))
        sa.net.load_state_dict(torch.load(path))
        sa.net.eval()
        return sa

    def train(self, data_file, save_path, mask_target=False):
        train_data = BertDataset.from_file(data_file, mask_target=mask_target)
        train_loader = DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=True, num_workers=4,
                                  collate_fn=generate_batch)

        self.net = TDBertNet(len(polarity_indices))

        # initialise GPU
        self.net.cuda()

        optimiser = optim.Adam(self.net.parameters(), lr=LEARNING_RATE)

        start = time.time()

        for epoch in range(MAX_EPOCHS):
            batch_loss = 0.0
            for i, batch in enumerate(train_loader):
                # send batch to gpu
                texts, target_indices, labels = tuple(i.to(device) for i in batch)

                # zero param gradients
                optimiser.zero_grad()

                # forward pass
                outputs, _ = self.net(texts, target_indices)

                # backward pass
                l = loss(outputs, labels)
                l.backward()

                # optimise
                optimiser.step()

                # print interim stats every 10 batches
                batch_loss += l.item()
                if i % 10 == 9:
                    print('epoch:', epoch + 1, '-- batch:', i + 1, '-- avg loss:', batch_loss / 10)
                    batch_loss = 0.0

        end = time.time()
        print('Training took', end - start, 'seconds')

        torch.save(self.net.state_dict(), save_path)

    def evaluate(self, data_file, mask_target=False):
        test_data = BertDataset.from_file(data_file, mask_target=mask_target)
        test_loader = DataLoader(test_data, batch_size=BATCH_SIZE, shuffle=False, num_workers=4,
                                 collate_fn=generate_batch)

        predicted = []
        truths = []
        with torch.no_grad():
            for (texts, target_indices, labels) in test_loader:
                outputs, attentions = self.net(texts, target_indices)
                _, pred = torch.max(outputs.data, 1)
                predicted += pred.tolist()
                truths += labels.tolist()

        correct = (np.array(predicted) == np.array(truths))
        accuracy = correct.sum() / correct.size
        print('accuracy:', accuracy)

        cm = metrics.confusion_matrix(truths, predicted, labels=range(len(polarity_indices)))
        print('confusion matrix:')
        print(cm)

        f1 = metrics.f1_score(truths, predicted, labels=range(len(polarity_indices)), average='macro')
        print('macro F1:', f1)

    def get_batch_sentiment_polarity(self, data):
        dataset = BertDataset.from_data(data)
        loader = DataLoader(dataset, batch_size=128, shuffle=False, num_workers=8, collate_fn=generate_batch)

        self.net.cuda()
        self.net.eval()

        predicted = []
        with torch.no_grad():
            for input_ids, attn_mask, target_indices, _ in loader:
                input_ids, attn_mask, target_indices = tuple(i.to(device) for i in [input_ids, attn_mask, target_indices])
                outputs = self.net(input_ids, attn_mask, target_indices)
                batch_val, batch_pred = torch.max(outputs.data, 1)
                predicted += [BertAnalyzer.get_polarity(val, pred) for val, pred in zip(batch_val, batch_pred)]

        return predicted

    def get_sentiment_polarity(self, text, char_from, char_to):
        instance = Instance(text, char_from, char_to)
        tokens, tg_from, tg_to = instance.get(mask_target=False)
        text, target_indices = instance.to_tensor()

        with torch.no_grad():
            outputs, attentions = self.net(text, target_indices)

        # attention_heads = attentions[0]
        # num_heads = len(attention_heads)
        # ax = plt.subplot(111)
        # token_width = 1
        # head_width = token_width / num_heads
        # for i, head in enumerate(attention_heads):
        #     # plot attention histogram
        #     att_values = torch.mean(head[tg_from+1:tg_to+2], 0)[1:-1].numpy()
        #
        #     bins = [x - token_width / 2 + i * head_width for x in range(1, len(att_values) + 1)]
        #     ax.bar(bins, att_values, width=head_width)
        #     ax.set_xticks(list(range(1, len(att_values) + 1)))
        #     ax.set_xticklabels(tokens, rotation=45, rotation_mode='anchor', ha='right')
        # plt.show()

        val, pred = torch.max(outputs.data, 1)
        return BertAnalyzer.get_polarity(val, pred)

    @staticmethod
    def get_polarity(val, pred):
        if pred == 0:
            # positive
            return val
        elif pred == 1:
            # negative
            return -val
        else:
            # neutral or conflicted
            return 0
