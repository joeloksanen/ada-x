from agent.argumentquery import ArgumentQuery
from agent.framework import Framework
from agent.message import ADAMessage, ADAText


class Communicator:

    queries = [
        ArgumentQuery(0, 'Why was the {arg} highly rated?'),
        ArgumentQuery(1, 'Why was the {arg} poorly rated?'),
        ArgumentQuery(2, 'Why was the {arg} considered to be good?'),
        ArgumentQuery(3, 'Why was the {arg} considered to be poor?'),
        ArgumentQuery(4, 'What did users say about the {arg} being good?'),
        ArgumentQuery(5, 'What did users say about the {arg} being poor?'),
    ]

    def __init__(self, product_id):
        self.framework = Framework.load_saved(product_id)
        self.product = self.framework.product

    def get_init_message(self):
        prod_node = self.product.root
        prod = self.product.argument_for_node(prod_node)
        text = ADAText('What would you like to know about this *?', [prod.name])
        queries = self.get_queries(prod_node)
        args = [prod.with_queries(queries)]
        return ADAMessage(text, args)

    def get_response(self, query_id, arg_id):
        q_arg_node = self.product.argument_node_for_id(arg_id)
        q_arg = self.product.argument_for_id(arg_id)

        if query_id == 0:
            supp_node = self.framework.get_strongest_supporting_subfeature(q_arg_node)
            att_node = self.framework.get_strongest_attacking_subfeature(q_arg_node)
            supp_name = self.product.argument_for_node(supp_node).name
            text = ADAText('The * was highly rated because the * {} good'.format(self.was_were(supp_node)),
                           [q_arg.name, supp_name])
            if att_node:
                att_name = self.product.argument_for_node(att_node).name
                text.add(', although the * {} poor.'.format(self.was_were(att_node)), [att_name])
                args = [q_arg_node, supp_node, att_node]
            else:
                text.add('.')
                args = [q_arg_node, supp_node]

        if query_id == 2:
            supp_node = self.framework.get_strongest_supporting_subfeature(q_arg_node)
            att_node = self.framework.get_strongest_attacking_subfeature(q_arg_node)
            supp_name = self.product.argument_for_node(supp_node).name
            text = ADAText('The * was considered to be good because the * {} good'.format(self.was_were(supp_node)),
                           [q_arg.name, supp_name])
            if att_node:
                att_name = self.product.argument_for_node(att_node).name
                text.add(', although the * {} poor.'.format(self.was_were(att_node)), [att_name])
                args = [q_arg_node, supp_node, att_node]
            else:
                text.add('.')
                args = [q_arg_node, supp_node]

        if query_id == 3:
            supp_node = self.framework.get_strongest_supporting_subfeature(q_arg_node)
            supp_name = self.product.argument_for_node(supp_node).name
            att_node = self.framework.get_strongest_attacking_subfeature(q_arg_node)
            text = ADAText('The * was considered to be poor because the * {} poor'.format(self.was_were(supp_node)),
                           [q_arg.name, supp_name])
            if supp_node:
                att_name = self.product.argument_for_node(att_node).name
                text.add(', although the * {} good.'.format(self.was_were(att_node)), [att_name])
                args = [q_arg_node, supp_node, att_node]
            else:
                text.add('.')
                args = [q_arg_node, supp_node]

        if query_id == 4 or query_id == 5:
            phrase = (self.framework.best_supporting_phrase(q_arg_node) if query_id == 4
                      else self.framework.best_attacking_phrase(q_arg_node))

            template = ''
            args = []
            i = 0
            for form, start, end in phrase.get_arg_mentions(q_arg_node):
                template += phrase.text[i:start] + '*'
                i = end
                args.append(form)
            template += phrase.text[i:len(phrase.text)]

            while template[-1] == '.':
                template = template[:-1]

            text = ADAText('\"...{}...\"'.format(template), args, style='QUOT')
            args = [q_arg_node]

        args = [self.product.argument_for_node(arg).with_queries(self.get_queries(arg)) for arg in args]
        return ADAMessage(text, args)

    def get_queries(self, arg_node):
        arg = self.product.argument_for_node(arg_node)
        queries = []

        base = 0 if arg.id == 0 else 2

        if self.framework.liked_argument(arg_node):
            if self.framework.supported_argument(arg_node):
                queries.append(self.queries[base].with_argument(arg))
            supp_phrase = self.framework.best_supporting_phrase(arg_node)
            if supp_phrase:
                queries.append(self.queries[4].with_argument(arg))
        else:
            if self.framework.attacked_argument(arg_node):
                queries.append(self.queries[base + 1].with_argument(arg))
            att_phrase = self.framework.best_attacking_phrase(arg_node)
            if att_phrase:
                queries.append(self.queries[5].with_argument(arg))

        return queries

    def was_were(self, arg_n):
        return 'was' if self.product.singularities[arg_n] else 'were'

    def get_argument_graph(self):
        return self.framework.get_argument_graph()
