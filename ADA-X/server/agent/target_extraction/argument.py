class Argument:

    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.queries = []

    def with_queries(self, queries):
        arg = Argument(self.id, self.name)
        arg.queries = queries
        return arg
