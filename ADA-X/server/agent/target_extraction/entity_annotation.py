import pandas as pd
from gensim.models.phrases import Phrases, Phraser
from nltk import pos_tag
from nltk.tokenize import word_tokenize, sent_tokenize
import string
from nltk.corpus import stopwords
import re
from collections import Counter
import pickle
import os
import readchar
import random
from sty import fg, bg
from anytree import Node, RenderTree, LevelOrderIter, PreOrderIter

PHRASE_THRESHOLD = 4
ROW_CHARACTER_COUNT = 100
stop_words = stopwords.words('english')
ann_bgs = [bg.blue, bg.red]  # child, parent


class EntityAnnotator:

    def __init__(self, text_file_path, counter, phraser, save_path):
        self.text_file_path = text_file_path
        self.counter = counter
        self.phraser = phraser
        self.save_path = save_path
        self.root = None
        self.synset = {}
        self.n_annotated = 0

    @staticmethod
    def new_from_tsv(file_path, name):
        df = pd.read_csv(file_path, sep='\t', error_bad_lines=False)
        print('tokenizing texts...')
        texts = [text.replace('_', ' ')
                 for _, par in df.sample(frac=1)['reviewText'].items() if not pd.isnull(par)
                 for text in sent_tokenize(par)]
        print('obtaining counter...')
        counter, phraser = EntityAnnotator.count_nouns(texts)
        print('finished initialising annotator')
        ann = EntityAnnotator(file_path, counter, phraser, 'annotators/{}.pickle'.format(name))
        ann.save()
        return ann

    @staticmethod
    def load_saved(file_path):
        f = open(file_path, 'rb')
        ann = pickle.load(f)
        f.close()
        return ann

    def save(self):
        f = open(self.save_path, 'wb')
        pickle.dump(self, f)
        f.close()

    @staticmethod
    def count_nouns(raw_texts):
        texts = [word_tokenize(text) for text in raw_texts]
        print('  obtaining phraser...')
        # obtain phraser
        bigram = Phrases(texts, threshold=PHRASE_THRESHOLD)
        trigram = Phrases(bigram[texts], threshold=PHRASE_THRESHOLD)
        phraser = Phraser(trigram)

        print('  counting nouns...')
        # count nouns
        nouns = []
        for idx, text in enumerate(texts):
            pos_tags = pos_tag(text)
            ngrams = phraser[text]

            word_idx = 0
            for token in ngrams:
                if '_' in token:
                    words = token.split('_')
                    word_range = range(word_idx, word_idx + len(words))
                    has_noun = any(EntityAnnotator.is_noun(pos_tags[i]) for i in word_range)
                    all_terms_valid = all(EntityAnnotator.is_valid_term(pos_tags[i]) for i in word_range)
                    if has_noun and all_terms_valid:
                        nouns.append(token)
                    word_idx += len(words)
                else:
                    is_noun = EntityAnnotator.is_noun(pos_tags[word_idx])
                    is_valid = EntityAnnotator.is_valid_term(pos_tags[word_idx])
                    if len(token) > 1 and is_noun and is_valid:
                        nouns.append(token)
                    word_idx += 1
            if idx % 1000 == 0:
                print('    {:0.2f} done'.format((idx + 1) / len(texts)))

        return Counter(nouns), phraser

    @staticmethod
    def is_noun(pos_tagged):
        word, tag = pos_tagged
        return tag.startswith('NN') and word.lower() not in string.punctuation and word not in stop_words

    # true if term is not a preposition and does not include special characters
    @staticmethod
    def is_valid_term(pos_tagged):
        alpha_numeric_pat = '^\w+$'
        word, tag = pos_tagged
        return tag != 'IN' and re.match(alpha_numeric_pat, word)

    def annotate(self):
        while True:
            entity = self.select_entity()

            os.system('clear')

            print(fg.li_green + '{} nouns annotated'.format(self.n_annotated) + fg.rs)
            print('')

            print(fg.li_black + 'root:       \'r\'' + fg.rs)
            print(fg.li_black + 'subfeat:    [\'f\'][number of parent node][ENTER]' + fg.rs)
            print(fg.li_black + 'synonym:    [\'s\'][number of syn node][ENTER]' + fg.rs)
            print(fg.li_black + 'nan:        \'n\'' + fg.rs)
            print(fg.li_black + 'remove:     \'x\'' + fg.rs)
            print(fg.li_black + 'quit:       \'q\'' + fg.rs)
            print(fg.li_black + 'abort:      \'a\'' + fg.rs)
            print('')

            if self.root is not None:
                print(fg.li_blue + str(RenderTree(self.root)) + fg.rs)

            print('')

            print(entity)

            print('')

            task = readchar.readkey()

            if task == 'r':
                node = Node(entity)
                self.synset[node] = [node.name]
                old_root = self.root
                self.root = node
                if old_root is not None:
                    old_root.parent = self.root
                self.update_tree_indices()
                self.n_annotated += 1

            if task == 'f':
                n = None
                while True:
                    subtask = readchar.readkey()
                    if subtask.isdigit():
                        n = n * 10 + int(subtask) if n is not None else int(subtask)
                    if subtask == readchar.key.ENTER and n is not None:
                        node = Node(entity, parent=self.node_with_number(n))
                        self.synset[node] = [node.name]
                        self.update_tree_indices()
                        self.n_annotated += 1
                        break
                    if subtask == 'a':
                        break

            if task == 's':
                n = None
                while True:
                    subtask = readchar.readkey()
                    if subtask.isdigit():
                        n = n * 10 + int(subtask) if n is not None else int(subtask)
                    if subtask == readchar.key.ENTER and n is not None:
                        self.synset[self.node_with_number(n)].append(entity)
                        self.n_annotated += 1
                        break
                    if subtask == 'a':
                        break

            if task == 'x':
                n = None
                while True:
                    subtask = readchar.readkey()
                    if subtask.isdigit():
                        n = n * 10 + int(subtask) if n is not None else int(subtask)
                    if subtask == readchar.key.ENTER and n is not None:
                        node = self.node_with_number(n)
                        del self.synset[node]
                        del node
                        break
                    if subtask == 'a':
                        break

            if task == 'n':
                self.n_annotated += 1

            if task == 'q':
                break

        self.save()

    def select_entity(self):
        entity, _ = self.counter.most_common(self.n_annotated+1)[-1]
        return entity.replace('_', ' ')

    def node_with_number(self, n):
        return list(LevelOrderIter(self.root))[n]

    def update_tree_indices(self):
        i = 0
        for node in LevelOrderIter(self.root):
            node.n = i
            i += 1

    def save_annotated_pairs(self, save_path):
        reviews = pd.read_csv(self.text_file_path, sep='\t', error_bad_lines=False)
        texts = [text for _, par in reviews['reviewText'].items() if not pd.isnull(par)
                 for text in sent_tokenize(par)]

        pair_texts = [t for t in map(lambda t: self.pair_relations_for_text(t), texts)
                      if t is not None]

        df = pd.DataFrame(pair_texts, columns=['sentText', 'relationMentions'])
        df.to_csv(save_path, sep='\t', index=False)

    def save_annotated_entities(self, save_path):
        reviews = pd.read_csv(self.text_file_path, sep='\t', error_bad_lines=False)
        texts = [text for _, par in reviews['reviewText'].items() if not pd.isnull(par)
                 for text in sent_tokenize(par)]

        all_entities = {(e, True) for e in self.get_annotated_entities()}.union(
            {(e, False) for e in self.get_nan_entities()})

        entity_texts = [t for t in map(lambda t: self.entity_mentions_in_text(t, all_entities), texts)
                        if t is not None]

        df = pd.DataFrame(entity_texts, columns=['sentText', 'entityMentions'])
        df.to_csv(save_path, sep='\t', index=False)

    @staticmethod
    def get_entity_relation(mention1, mention2):
        n1, e1 = mention1
        n2, e2 = mention2

        if n2 in n1.descendants:
            return {'em1Text': e1, 'em2Text': e2, 'label': '/has_feature'}
        elif n1 in n2.descendants:
            return {'em1Text': e2, 'em2Text': e1, 'label': '/has_feature'}
        else:
            # randomise order of no rel tuple to avoid bias
            m = [e1, e2]
            random.shuffle(m)
            return {'em1Text': m[0], 'em2Text': m[1], 'label': '/no_relation'}

    def pair_relations_for_text(self, text, nan_entities=None):
        single_tokens = word_tokenize(text)
        tagged_single = pos_tag(single_tokens)
        tagged_all = set().union(*[tagged_single, pos_tag(self.phraser[single_tokens])])

        entity_mentions = []
        for n in PreOrderIter(self.root):
            cont, mention = self.mention_in_text(tagged_all, node=n)
            if not cont:
                # many mentions of same entity
                return None
            if mention is not None:
                entity_mentions.append((n, mention))
                if len(entity_mentions) > 2:
                    # text cannot have more than two entity mentions
                    return None

        if len(entity_mentions) == 2:
            return text, [EntityAnnotator.get_entity_relation(entity_mentions[0], entity_mentions[1])]

        if nan_entities is not None and len(entity_mentions) == 1:
            nan_mention = None
            for term in nan_entities:
                cont, mention = self.mention_in_text(tagged_all, term=term)
                if not cont:
                    # many mentions of term
                    return None
                if mention is not None:
                    if nan_mention is not None:
                        # text cannot have more than one nan mention
                        return None
                    nan_mention = mention

            if nan_mention is not None:
                return text, [{'em1Text': entity_mentions[0][1], 'em2Text': nan_mention, 'label': '/no_relation'}]

        return None

    # returns True, (synonym of node / term / None) if there is exactly one or zero such occurrence,
    # otherwise False, None, None
    def mention_in_text(self, tagged_tokens, node=None, term=None):
        mention = None
        for syn in ({syn.lower() for syn in self.synset[node]} if node is not None else {term}):
            n_matches = sum(1 for token, tag in tagged_tokens
                            if syn == token.lower().replace('_', ' ') and tag.startswith('NN'))
            if n_matches > 1:
                return False, None
            if n_matches == 1:
                if mention is None:
                    mention = syn
                else:
                    return False, None
        return True, mention

    def entity_mentions_in_text(self, text, all_entities):
        single_tokens = word_tokenize(text)
        tagged_single = pos_tag(single_tokens)
        tagged_all = set().union(*[tagged_single, pos_tag(self.phraser[single_tokens])])

        entity_mention = None
        for entity, is_aspect in all_entities:
            cont, mention = self.mention_in_text(tagged_all, term=entity)
            if not cont:
                # many mentions of same entity
                return None
            if mention is not None:
                if entity_mention is None:
                    entity_mention = (entity, is_aspect)
                else:
                    # text cannot have more than one entity mention
                    return None

        if entity_mention is not None:
            return text, [{'text': entity_mention[0], 'label': 'ASPECT' if entity_mention[1] else 'NAN'}]

        return None

    def get_annotated_entities(self):
        return {syn.lower() for n in PreOrderIter(self.root) for syn in self.synset[n]}

    def get_nan_entities(self):
        annotated = self.get_annotated_entities()
        return {t.replace('_', ' ').lower() for t, _ in self.counter.most_common(self.n_annotated)
                if t.replace('_', ' ').lower() not in annotated}

    def save_annotated_texts(self, save_path):
        reviews = pd.read_csv(self.text_file_path, sep='\t', error_bad_lines=False)
        texts = [text for _, par in reviews.sample(frac=1)['reviewText'].items() if not pd.isnull(par)
                 for text in sent_tokenize(par)]

        labelled_texts = [t for t in map(self.relations_for_text, texts) if t is not None]

        df = pd.DataFrame(labelled_texts, columns=['sentText', 'relationMentions'])
        df.to_csv(save_path, sep='\t', index=False)

    def relations_for_text(self, text):
        rels = []
        child_entities = []
        for e1 in PreOrderIter(self.root):
            if not e1.is_leaf and e1.name in text:
                for e2 in e1.children:
                    if e2.name in text:
                        # e1 is a parent of an entity in the text
                        if e1 in child_entities:
                            # e1 cannot be a parent and a child
                            return None
                        rels.append({'em1Text': e1.name, 'em2Text': e2.name, 'label': '/has_feature'})
                        child_entities.append(e2)
        return text, rels


ea: EntityAnnotator = EntityAnnotator.load_saved('annotators/watch_annotator.pickle')
ea.save_annotated_pairs('BERT/data/annotated_watch_review_pairs.tsv')
ea.save_annotated_entities('BERT/data/annotated_watch_review_entities.tsv')
