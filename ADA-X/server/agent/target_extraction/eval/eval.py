import pandas as pd


methods = ['Ours', 'WordNet', 'COMeT']
products = ['watch', 'tv', 'necklace', 'mixer', 'game']
n_raters = 3


def get_df(product, i):
    return pd.read_csv('data/{}_{}.tsv'.format(product, i), sep='\t', index_col=False)


def get_votes(df):
    return df.apply(lambda row: row['true'] > row['false'], axis=1)


def get_votes_for_product(product):
    for i in range(n_raters):
        votes = get_votes(get_df(product, i))
        print(methods[i])
        print(votes)
        print('')


def get_accuracy(df):
    votes = get_votes(df)
    return sum(1 if vote else 0 for vote in votes) / len(votes) if len(votes) > 0 else None


def print_accuracies():
    for prod in products:
        print(prod)
        for i in range(3):
            df = get_df(prod, i)
            print('  {}: {}'.format(methods[i], get_accuracy(df)))

    print('total')
    for i in range(3):
        df = pd.concat(get_df(prod, i) for prod in products)
        print('  {}: {}'.format(methods[i], get_accuracy(df)))


def get_kappa():
    df = pd.concat(get_df(prod, i) for prod in products for i in range(3)).reset_index(drop=True)
    votes = get_votes(df)
    pr_true = sum(1 if vote else 0 for vote in votes) / len(votes)

    pe = pr_true ** 2 + (1 - pr_true) ** 2

    n_subj = len(df)
    p = df.apply(lambda row: (row['true'] ** 2 + row['false'] ** 2 - n_raters) / (n_raters*(n_raters-1)), axis=1).sum() / n_subj

    return (p - pe) / (1 - pe)


def get_agreement():
    df = pd.concat(get_df(prod, i) for prod in products for i in range(3)).reset_index(drop=True)
    agreed = df[df.apply(lambda row: row['true'] == n_raters or row['false'] == n_raters, axis=1)]
    return len(agreed) / len(df)


for p in products:
    print(len(get_df(p, 0)))