import requests
import threading
import time


class ConceptNet:

    url = 'http://api.conceptnet.io'
    limit = 50

    def find_related(self, feature, rel):
        uri = '/query?node=/c/en/{feature}&other=/c/en&rel=/r/{rel}&limit={limit}'.format(feature=feature, rel=rel, limit=self.limit)
        obj = requests.get(self.url + uri).json()
        unique = set([obj['edges'][i]['end']['label'] for i in range(len(obj['edges']))])
        return unique

    def find_relations(self, f1, f2):
        uri = '/query?node=/c/en/{f1}&other=/c/en/{f2}'.format(f1=f1, f2=f2)
        obj = requests.get(self.url + uri).json()
        return obj

    def get_relatedness(self, f1, f2):
        uri = '/relatedness?node1=/c/en/{f1}&node2=/c/en/{f2}'.format(f1=f1.replace(' ','_'), f2=f2.replace(' ','_'))
        obj = requests.get(self.url + uri).json()
        time.sleep(0.5)  # only 3600 requests allowed / hour
        return obj['value']

    def append_result(self, feature, rel, result_set, lock):
        rels = self.find_related(feature, rel)
        lock.acquire()
        result_set.update(rels)
        lock.release()

    def parent_check(self, node, parent, synonyms):
        if parent == None:
            return
        min_r = 0.1
        ratio = 1.2 # relatedness for child has to be at least twice as high as for parent
        rm = set()
        for s in synonyms:
            r_child = self.get_relatedness(node.name, s)
            r_parent = self.get_relatedness(parent.name, s)
            if (r_child < min_r) or (r_parent < min_r) or (r_parent != 0 and r_child / r_parent < ratio):
                rm.add(s)
        synonyms.difference_update(rm)
        self.parent_check(node, parent.parent, synonyms)

    def sem_synonyms_for_node(self, node):
        rels = ['DefinedAs', 'Synonym', 'IsA', 'RelatedTo']  # SimilarTo? FormOf?

        synonyms = set()
        lock = threading.Lock()

        threads = []
        for rel in rels:
            t = threading.Thread(target=self.append_result, args=(node.name, rel, synonyms, lock))
            t.start()
            threads.append(t)
        for t in threads:
            t.join()

        self.parent_check(node, node.parent, synonyms)

        synonyms.add(node.name)
        return synonyms

    def sub_features_for_argument(self, argument):
        rels = ['UsedFor', 'HasA', 'CapableOf', 'Causes', 'HasSubevent', 'HasProperty', 'MadeOf']

        features = set()
        lock = threading.Lock()

        threads = []
        for rel in rels:
            t = threading.Thread(target=self.append_result, args=(argument, rel, features, lock))
            t.start()
            threads.append(t)
        for t in threads:
            t.join()

        return features
