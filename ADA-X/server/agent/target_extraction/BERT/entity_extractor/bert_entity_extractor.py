import torch
from torch.nn.utils import clip_grad_norm_
from torch.utils.data import DataLoader
from torch.nn.functional import softmax
from torch.nn import CrossEntropyLoss
from torch.optim import Adam
import time
import numpy as np
from sklearn import metrics
import statistics
from transformers import get_linear_schedule_with_warmup
from agent.target_extraction.BERT.entity_extractor.entity_dataset import EntityDataset, generate_batch, generate_production_batch
from agent.target_extraction.BERT.entity_extractor.entitybertnet import NUM_CLASSES, EntityBertNet

device = torch.device('cuda')

# optimizer
DECAY_RATE = 0.01
LEARNING_RATE = 0.00002
MAX_GRAD_NORM = 1.0

# training
N_EPOCHS = 3
BATCH_SIZE = 32
WARM_UP_FRAC = 0.05

# loss
loss_criterion = CrossEntropyLoss()


class BertEntityExtractor:

    def __init__(self):
        self.net = EntityBertNet()

    @staticmethod
    def load_saved(path):
        extr = BertEntityExtractor()
        extr.net = EntityBertNet()
        extr.net.load_state_dict(torch.load(path))
        extr.net.eval()
        return extr

    @staticmethod
    def new_trained_with_file(file_path, save_path, size=None):
        extractor = BertEntityExtractor()
        extractor.train_with_file(file_path, save_path, size=size)
        return extractor

    @staticmethod
    def train_and_validate(file_path, save_file, size=None, valid_frac=None, valid_file_path=None):
        extractor = BertEntityExtractor()
        extractor.train_with_file(file_path, save_file, size=size, valid_frac=valid_frac,
                                  valid_file_path=valid_file_path)
        return extractor

    def train_with_file(self, file_path, save_file, size=None, valid_frac=None, valid_file_path=None):
        # load training data
        if valid_file_path is None:
            train_data, valid_data = EntityDataset.from_file(file_path, size=size, valid_frac=valid_frac)
        else:
            train_size = int(size * (1 - valid_frac)) if size is not None else None
            train_data, _ = EntityDataset.from_file(file_path, size=train_size)
            valid_size = int(size * valid_frac) if size is not None else int(len(train_data) * valid_frac)
            valid_data, _ = EntityDataset.from_file(valid_file_path, size=valid_size)
        train_loader = DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=True, num_workers=4,
                                  collate_fn=generate_batch)

        # initialise BERT
        self.net.cuda()

        # set up optimizer with weight decay
        optimiser = Adam(self.net.parameters(), lr=LEARNING_RATE)

        # set up scheduler for lr
        n_training_steps = len(train_loader) * N_EPOCHS
        scheduler = get_linear_schedule_with_warmup(
            optimiser,
            num_warmup_steps=int(WARM_UP_FRAC * n_training_steps),
            num_training_steps=n_training_steps
        )

        start = time.time()

        for epoch_idx in range(N_EPOCHS):
            self.net.train()
            batch_loss = 0.0

            for batch_idx, batch in enumerate(train_loader):
                # send batch to gpu
                input_ids, attn_mask, entity_indices, target_labels = tuple(i.to(device) for i in batch)

                # zero param gradients
                optimiser.zero_grad()

                # forward pass
                output_scores = self.net(input_ids, attn_mask, entity_indices)

                # backward pass
                loss = loss_criterion(output_scores, target_labels)
                loss.backward()

                # clip gradient norm
                clip_grad_norm_(parameters=self.net.parameters(), max_norm=MAX_GRAD_NORM)

                # optimise
                optimiser.step()

                # update lr
                scheduler.step()

                # print interim stats every 250 batches
                batch_loss += loss.item()
                if batch_idx % 250 == 249:
                    batch_no = batch_idx + 1
                    print('epoch:', epoch_idx + 1, '-- progress: {:.4f}'.format(batch_no / len(train_loader)),
                          '-- batch:', batch_no, '-- avg loss:', batch_loss / 250)
                    batch_loss = 0.0

            print('epoch done')

            if valid_data is not None:
                self.evaluate(data=valid_data)

        torch.save(self.net.state_dict(), '{}.pt'.format(save_file))

        end = time.time()
        print('Training took', end - start, 'seconds')

    def evaluate(self, file_path=None, data=None, size=None):
        # load eval data
        if file_path is not None:
            test_data, _ = EntityDataset.from_file(file_path, size=size)
        else:
            if data is None:
                raise AttributeError('file_path and data cannot both be None')
            test_data = data

        test_loader = DataLoader(test_data, batch_size=BATCH_SIZE, shuffle=False, num_workers=4,
                                 collate_fn=generate_batch)

        self.net.cuda()
        self.net.eval()

        outputs = []
        targets = []

        with torch.no_grad():
            for batch in test_loader:
                # send batch to gpu
                input_ids, attn_mask, entity_indices, target_labels = tuple(i.to(device) for i in batch)

                # forward pass
                output_scores = self.net(input_ids, attn_mask, entity_indices)
                _, output_labels = torch.max(output_scores.data, 1)

                outputs += output_labels.tolist()
                targets += target_labels.tolist()

        assert len(outputs) == len(targets)

        correct = (np.array(outputs) == np.array(targets))
        accuracy = correct.sum() / correct.size
        print('accuracy:', accuracy)

        cm = metrics.confusion_matrix(targets, outputs, labels=range(NUM_CLASSES))
        print('confusion matrix:')
        print(cm)

        f1 = metrics.f1_score(targets, outputs, labels=range(NUM_CLASSES), average='macro')
        print('macro F1:', f1)

        precision = metrics.precision_score(targets, outputs, average=None)
        print('precision:', precision)

        recall = metrics.recall_score(targets, outputs, average=None)
        print('recall:', recall)

    def extract_entity_probabilities(self, terms, file_path=None, dataset=None, size=None):
        # load data
        if file_path is not None:
            data, _ = EntityDataset.from_file(file_path, size=size)
        else:
            if dataset is None:
                raise AttributeError('file_path and data cannot both be None')
            data = dataset

        loader = DataLoader(data, batch_size=BATCH_SIZE, shuffle=False, num_workers=4,
                            collate_fn=generate_production_batch)

        self.net.cuda()
        self.net.eval()

        probs = {term: [] for term in terms}

        with torch.no_grad():
            for input_ids, attn_mask, entity_indices, instances in loader:
                # send batch to gpu
                input_ids, attn_mask, entity_indices = tuple(i.to(device) for i in [input_ids, attn_mask,
                                                                                    entity_indices])

                # forward pass
                output_scores = softmax(self.net(input_ids, attn_mask, entity_indices), dim=1)
                entity_scores = output_scores.narrow(1, 1, 1).flatten()

                for ins, score in zip(instances, entity_scores.tolist()):
                    probs[ins.entity].append(score)

        return {t: statistics.mean(t_probs) if len(t_probs) > 0 else None for t, t_probs in probs.items()}
