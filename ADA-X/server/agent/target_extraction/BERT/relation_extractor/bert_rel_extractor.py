import torch
from torch.nn.utils import clip_grad_norm_
from torch.utils.data import DataLoader
from torch.nn.functional import softmax
from torch.nn import CrossEntropyLoss
from torch.optim import Adam
import time
import numpy as np
from sklearn import metrics
from transformers import get_linear_schedule_with_warmup
from agent.target_extraction.BERT.relation_extractor.pair_rel_dataset import PairRelDataset, generate_batch, generate_production_batch
from agent.target_extraction.BERT.relation_extractor.pairbertnet import NUM_CLASSES, PairBertNet

device = torch.device('cuda')

# optimizer
DECAY_RATE = 0.01
LEARNING_RATE = 0.00002
MAX_GRAD_NORM = 1.0

# training
N_EPOCHS = 3
BATCH_SIZE = 16
WARM_UP_FRAC = 0.05

# loss
loss_criterion = CrossEntropyLoss()


class BertRelExtractor:

    def __init__(self):
        self.net = PairBertNet()

    @staticmethod
    def load_saved(path):
        extr = BertRelExtractor()
        extr.net = PairBertNet()
        extr.net.load_state_dict(torch.load(path))
        extr.net.eval()
        return extr

    @staticmethod
    def new_trained_with_file(file_path, save_path, size=None):
        extractor = BertRelExtractor()
        extractor.train_with_file(file_path, save_path, size=size)
        return extractor

    @staticmethod
    def train_and_validate(file_path, save_file, size=None, valid_frac=None, valid_file_path=None):
        extractor = BertRelExtractor()
        extractor.train_with_file(file_path, save_file, size=size, valid_frac=valid_frac,
                                  valid_file_path=valid_file_path)
        return extractor

    def train_with_file(self, file_path, save_file, size=None, valid_frac=None, valid_file_path=None):
        # load training data
        if valid_file_path is None:
            train_data, valid_data = PairRelDataset.from_file(file_path, size=size, valid_frac=valid_frac)
        else:
            train_size = int(size * (1 - valid_frac)) if size is not None else None
            train_data, _ = PairRelDataset.from_file(file_path, size=train_size)
            valid_size = int(size * valid_frac) if size is not None else int(len(train_data) * valid_frac)
            valid_data, _ = PairRelDataset.from_file(valid_file_path, size=valid_size)
        train_loader = DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=True, num_workers=4,
                                  collate_fn=generate_batch)

        # initialise BERT
        self.net.cuda()

        # set up optimizer with weight decay
        optimiser = Adam(self.net.parameters(), lr=LEARNING_RATE)

        # set up scheduler for lr
        n_training_steps = len(train_loader) * N_EPOCHS
        scheduler = get_linear_schedule_with_warmup(
            optimiser,
            num_warmup_steps=int(WARM_UP_FRAC * n_training_steps),
            num_training_steps=n_training_steps
        )

        start = time.time()

        for epoch_idx in range(N_EPOCHS):
            self.net.train()
            batch_loss = 0.0

            for batch_idx, batch in enumerate(train_loader):
                # send batch to gpu
                input_ids, attn_mask, masked_indices, fst_indices, snd_indices, target_labels = tuple(i.to(device) for i in batch)

                # zero param gradients
                optimiser.zero_grad()

                # forward pass
                output_scores = self.net(input_ids, attn_mask, masked_indices, fst_indices, snd_indices)

                # backward pass
                loss = loss_criterion(output_scores, target_labels)
                loss.backward()

                # clip gradient norm
                clip_grad_norm_(parameters=self.net.parameters(), max_norm=MAX_GRAD_NORM)

                # optimise
                optimiser.step()

                # update lr
                scheduler.step()

                # print interim stats every 500 batches
                batch_loss += loss.item()
                if batch_idx % 500 == 499:
                    batch_no = batch_idx + 1
                    print('epoch:', epoch_idx + 1, '-- progress: {:.4f}'.format(batch_no / len(train_loader)),
                          '-- batch:', batch_no, '-- avg loss:', batch_loss / 500)
                    batch_loss = 0.0

            print('epoch done')

            if valid_data is not None:
                self.evaluate(data=valid_data)

        torch.save(self.net.state_dict(), '{}.pt'.format(save_file))

        end = time.time()
        print('Training took', end - start, 'seconds')

    def evaluate(self, file_path=None, data=None, size=None):
        # load eval data
        if file_path is not None:
            test_data, _ = PairRelDataset.from_file(file_path, size=size)
        else:
            if data is None:
                raise AttributeError('file_path and data cannot both be None')
            test_data = data

        test_loader = DataLoader(test_data, batch_size=BATCH_SIZE, shuffle=False, num_workers=4,
                                 collate_fn=generate_batch)

        self.net.cuda()
        self.net.eval()

        outputs = []
        targets = []

        with torch.no_grad():
            for batch in test_loader:
                # send batch to gpu
                input_ids, attn_mask, masked_indices, fst_indices, snd_indices, target_labels = tuple(i.to(device)
                                                                                                      for i in batch)

                # forward pass
                output_scores = self.net(input_ids, attn_mask, masked_indices, fst_indices, snd_indices)
                _, output_labels = torch.max(output_scores.data, 1)

                outputs += output_labels.tolist()
                targets += target_labels.tolist()

        assert len(outputs) == len(targets)

        correct = (np.array(outputs) == np.array(targets))
        accuracy = correct.sum() / correct.size
        print('accuracy:', accuracy)

        cm = metrics.confusion_matrix(targets, outputs, labels=range(NUM_CLASSES))
        print('confusion matrix:')
        print(cm)

        f1 = metrics.f1_score(targets, outputs, labels=range(NUM_CLASSES), average='macro')
        print('macro F1:', f1)

        precision = metrics.precision_score(targets, outputs, average=None)
        print('precision:', precision)

        recall = metrics.recall_score(targets, outputs, average=None)
        print('recall:', recall)

    def extract_single_relation(self, text, e1, e2):
        ins = PairRelDataset.get_instance(text, e1, e2)
        input_ids, attn_mask, masked_indices, prod_indices, feat_indices, instances = generate_production_batch([ins])

        self.net.cuda()
        self.net.eval()

        with torch.no_grad():
            # send batch to gpu
            input_ids, attn_mask, masked_indices, prod_indices, feat_indices = tuple(i.to(device) for i in
                                                                                     [input_ids, attn_mask,
                                                                                      masked_indices, prod_indices,
                                                                                      feat_indices])

            # forward pass
            output_scores = softmax(self.net(input_ids, attn_mask, masked_indices, prod_indices, feat_indices), dim=1)
            _, output_labels = torch.max(output_scores.data, 1)

            print(instances[0].get_relation_for_label(output_labels[0]))

    def extract_relations(self, n_aspects, aspect_index_map, aspect_counts, file_path=None, dataset=None, size=None):
        # load data
        if file_path is not None:
            data, _ = PairRelDataset.from_file(file_path, size=size)
        else:
            if dataset is None:
                raise AttributeError('file_path and data cannot both be None')
            data = dataset

        loader = DataLoader(data, batch_size=BATCH_SIZE, shuffle=False, num_workers=4,
                            collate_fn=generate_production_batch)

        self.net.cuda()
        self.net.eval()

        prob_matrix = np.zeros((n_aspects, n_aspects))
        count_matrix = np.zeros((n_aspects, n_aspects))

        with torch.no_grad():
            for input_ids, attn_mask, masked_indices, prod_indices, feat_indices, instances in loader:
                # send batch to gpu
                input_ids, attn_mask, masked_indices, prod_indices, feat_indices = tuple(i.to(device) for i in
                                                                                         [input_ids, attn_mask,
                                                                                          masked_indices, prod_indices,
                                                                                          feat_indices])

                # forward pass
                output_scores = softmax(self.net(input_ids, attn_mask, masked_indices, prod_indices, feat_indices), dim=1)
                rel_scores = output_scores.narrow(1, 1, 2)

                for ins, scores in zip(instances, rel_scores.tolist()):
                    forward_score, backward_score = scores
                    fst_idx, snd_idx = aspect_index_map[ins.fst], aspect_index_map[ins.snd]
                    prob_matrix[snd_idx][fst_idx] += forward_score
                    prob_matrix[fst_idx][snd_idx] += backward_score
                    count_matrix[snd_idx][fst_idx] += 1
                    count_matrix[fst_idx][snd_idx] += 1

        return prob_matrix, count_matrix


