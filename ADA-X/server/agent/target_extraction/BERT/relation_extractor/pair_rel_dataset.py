import torch
from torch.utils.data import Dataset
from transformers import BertTokenizer
import pandas as pd
import numpy as np
from ast import literal_eval
from agent.target_extraction.BERT.relation_extractor.pairbertnet import TRAINED_WEIGHTS, HIDDEN_OUTPUT_FEATURES
import os

MAX_SEQ_LEN = 128
RELATIONS = ['/has_feature', '/no_relation']
RELATION_LABEL_MAP = {None: None, '/has_feature': 1, '/no_relation': 0}
MASK_TOKEN = '[MASK]'
tokenizer = BertTokenizer.from_pretrained(TRAINED_WEIGHTS)


def generate_batch(batch):
    encoded = tokenizer.batch_encode_plus([instance.tokens for instance in batch], add_special_tokens=True,
                                          max_length=MAX_SEQ_LEN, pad_to_max_length=True, is_pretokenized=True,
                                          return_tensors='pt')
    input_ids = encoded['input_ids']
    attn_mask = encoded['attention_mask']
    labels = torch.tensor([instance.label for instance in batch])

    both_ranges = [instance.ranges for instance in batch]
    masked_indices = torch.tensor([[ins_idx, token_idx] for ins_idx, ranges in enumerate(both_ranges)
                                  for start, end in ranges for token_idx in range(start, end + 1)])
    fst_indices, snd_indices = map(indices_for_entity_ranges, zip(*both_ranges))

    return input_ids, attn_mask, masked_indices, fst_indices, snd_indices, labels


def generate_production_batch(batch):
    encoded = tokenizer.batch_encode_plus([instance.tokens for instance in batch], add_special_tokens=True,
                                          max_length=MAX_SEQ_LEN, pad_to_max_length=True, is_pretokenized=True,
                                          return_tensors='pt')
    input_ids = encoded['input_ids']
    attn_mask = encoded['attention_mask']

    both_ranges = [instance.ranges for instance in batch]
    masked_indices = torch.tensor([[ins_idx, token_idx] for ins_idx, ranges in enumerate(both_ranges)
                                   for start, end in ranges for token_idx in range(start, end + 1)])
    fst_indices, snd_indices = map(indices_for_entity_ranges, zip(*both_ranges))

    return input_ids, attn_mask, masked_indices, fst_indices, snd_indices, batch


def indices_for_entity_ranges(ranges):
    max_e_len = max(end - start for start, end in ranges)
    indices = torch.tensor([[[min(t, end)] * HIDDEN_OUTPUT_FEATURES
                             for t in range(start, start + max_e_len + 1)]
                            for start, end in ranges])
    return indices


class PairRelDataset(Dataset):

    def __init__(self, df, size=None):
        # filter inapplicable rows
        self.df = df[df.apply(lambda x: PairRelDataset.instance_from_row(x) is not None, axis=1)]

        # sample data if a size is specified
        if size is not None and size < len(self):
            self.df = self.df.sample(size, replace=False)

    @staticmethod
    def from_df(df, size=None):
        dataset = PairRelDataset(df, size=size)
        print('Obtained dataset of size', len(dataset))
        return dataset

    @staticmethod
    def from_file(file_name, valid_frac=None, size=None):
        f = open(os.path.dirname(__file__) + '/../data/' + file_name)
        if file_name.endswith('.json'):
            dataset = PairRelDataset(pd.read_json(f, lines=True), size=size)
        elif file_name.endswith('.tsv'):
            dataset = PairRelDataset(pd.read_csv(f, sep='\t', error_bad_lines=False), size=size)
        else:
            raise AttributeError('Could not recognize file type')

        if valid_frac is None:
            print('Obtained dataset of size', len(dataset))
            return dataset, None
        else:
            split_idx = int(len(dataset) * (1 - valid_frac))
            dataset.df, valid_df = np.split(dataset.df, [split_idx], axis=0)
            validset = PairRelDataset(valid_df)
            print('Obtained train set of size', len(dataset), 'and validation set of size', len(validset))
            return dataset, validset

    @staticmethod
    def instance_from_row(row):
        unpacked_arr = literal_eval(row['relationMentions']) if type(row['relationMentions']) is str else row['relationMentions']
        rms = [rm for rm in unpacked_arr if 'label' not in rm or rm['label'] in RELATIONS]
        if len(rms) == 1:
            prod, feat, rel = rms[0]['em1Text'], rms[0]['em2Text'], (rms[0]['label'] if 'label' in rms[0] else None)
        else:
            return None  # raise AttributeError('Instances must have exactly one relation')

        text = row['sentText']
        return PairRelDataset.get_instance(text, [prod, feat], relation=rel)

    @staticmethod
    def get_instance(text, entities, relation=None):
        tokens = tokenizer.tokenize(text)

        i = 0
        found_entities = []
        ranges = []
        while i < len(tokens):
            match = False
            for entity in entities:
                match_length = PairRelDataset.token_entity_match(i, entity.lower(), tokens)
                if match_length is not None:
                    if entity in found_entities:
                        return None  # raise AttributeError('Entity {} appears twice in text {}'.format(entity, text))
                    match = True
                    found_entities.append(entity)
                    tokens[i:i+match_length] = [MASK_TOKEN] * match_length
                    ranges.append((i + 1, i + match_length))  # + 1 taking into account the [CLS] token
                    i += match_length
                    break
            if not match:
                i += 1

        if len(found_entities) != 2:
            return None  # raise AttributeError('Could not find entities {} and {} in {}. Found entities {}'.format(e1, e2, text, found_entities))

        if relation is None:
            return PairRelInstance(tokens, found_entities[0], found_entities[1], ranges, None, text)

        if relation == '/has_feature':
            if found_entities == entities:
                return PairRelInstance(tokens, found_entities[0], found_entities[1], ranges, 1, text)
            else:
                assert found_entities == entities[::-1]
                return PairRelInstance(tokens, found_entities[0], found_entities[1], ranges, 2, text)

        assert relation == '/no_relation'
        return PairRelInstance(tokens, found_entities[0], found_entities[1], ranges, 0, text)

    @staticmethod
    def token_entity_match(first_token_idx, entity, tokens):
        token_idx = first_token_idx
        remaining_entity = entity
        while remaining_entity:
            if remaining_entity == entity or remaining_entity.lstrip() != remaining_entity:
                # start of new word
                remaining_entity = remaining_entity.lstrip()
                if token_idx < len(tokens) and tokens[token_idx] == remaining_entity[:len(tokens[token_idx])]:
                    remaining_entity = remaining_entity[len(tokens[token_idx]):]
                    token_idx += 1
                else:
                    break
            else:
                # continuing same word
                if (token_idx < len(tokens) and tokens[token_idx].startswith('##')
                        and tokens[token_idx][2:] == remaining_entity[:len(tokens[token_idx][2:])]):
                    remaining_entity = remaining_entity[len(tokens[token_idx][2:]):]
                    token_idx += 1
                else:
                    break
        if remaining_entity:
            return None
        else:
            return token_idx - first_token_idx

    def __len__(self):
        return len(self.df.index)

    def __getitem__(self, idx):
        return PairRelDataset.instance_from_row(self.df.iloc[idx])


class PairRelInstance:

    def __init__(self, tokens, fst, snd, ranges, label, text):
        self.tokens = tokens
        self.fst = fst
        self.snd = snd
        self.ranges = ranges
        self.label = label
        self.text = text

    def get_relation_for_label(self, label):
        if label == 0:
            return self.fst, '/no_relation', self.snd
        if label == 1:
            return self.fst, '/has_feature', self.snd
        if label == 2:
            return self.snd, '/has_feature', self.fst
