class Argument:

    def __init__(self, text, polarity, supporters, attackers, phrase, size):
        self.text = text
        self.polarity = 'POS' if polarity else 'NEG'
        self.supporters = supporters
        self.attackers = attackers
        self.phrase = phrase.text if phrase else '-'
        self.size = size
