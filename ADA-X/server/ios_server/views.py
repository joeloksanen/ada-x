from django.http import HttpResponse
import json
import jsonpickle
from django.views.decorators.csrf import csrf_exempt
from agent.dataloader import DataLoader
from agent.communicator import Communicator

communicators = []
product_ids = ['B0000TIKK8', 'B0000TIIPK', 'B000AYW0M2', 'B000AYW0KO', 'B004J30ERI', 'B004VR9HP2',
               'B00005UP2N', 'B0001HLTTI', 'B00063ULMI', 'B00791QYMQ']


class Empty:
    pass


def index(request):
    return HttpResponse("OK")


def products(request):
    product_infos = []
    for product_id in product_ids:
        product_title = DataLoader.get_product_name(product_id)
        star_rating = DataLoader.get_avg_star_rating(product_id)
        image_url = 'https://ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=US&ASIN=' + product_id + '&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=SL250'
        product_info = Empty()
        product_info.id = product_id
        product_info.name = product_title
        product_info.starRating = star_rating
        product_info.imageURL = image_url
        product_infos.append(product_info)

    return HttpResponse(jsonpickle.encode(product_infos, unpicklable=False), content_type="application/json")


def product(request):
    product_id = request.GET.get('id', '')

    if communicators:
        communicators.pop()
    communicators.append(Communicator(product_id))
    communicator = communicators[0]

    init_message = communicator.get_init_message()
    return HttpResponse(jsonpickle.encode(init_message, unpicklable=False), content_type="application/json")


@csrf_exempt
def message(request):
    parsed = json.loads(request.body)
    query_id = parsed['queryID']
    arg_id = parsed['argumentID']
    response = communicators[0].get_response(query_id, arg_id)

    return HttpResponse(jsonpickle.encode(response, unpicklable=False), content_type="application/json")


def arguments(request):
    product_id = request.GET.get('id', '')

    comm = Communicator(product_id)
    root = comm.get_argument_graph()

    product_title = DataLoader.get_product_name(product_id)
    star_rating = DataLoader.get_avg_star_rating(product_id)
    image_url = 'https://ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=US&ASIN=' + product_id + '&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=SL250'
    product_info = Empty()
    product_info.id = product_id
    product_info.name = product_title
    product_info.starRating = star_rating
    product_info.imageURL = image_url

    data = Empty()
    data.root = root
    data.info = product_info

    return HttpResponse(jsonpickle.encode(data, unpicklable=False), content_type="application/json")

