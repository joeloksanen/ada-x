from django.apps import AppConfig


class IosServerConfig(AppConfig):
    name = 'ios_server'
