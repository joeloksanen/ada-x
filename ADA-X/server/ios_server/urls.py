from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('products/', views.products, name='products'),
    path('product/', views.product, name='product'),
    path('message/', views.message, name='message'),
    path('arguments/', views.arguments, name='arguments')
]
