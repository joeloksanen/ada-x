//
//  ContentView.swift
//  ADAgraph
//
//  Created by Joel Oksanen on 13.6.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @ObservedObject var connectionManager: ConnectionManager
  @ObservedObject var selection: SelectionHandler
  
  var body: some View {
    VStack {
      if connectionManager.graph != nil && connectionManager.product != nil {
        SurfaceView(graph: connectionManager.graph, selection: selection, connectionManager: connectionManager)
      } else {
        Text("Loading arguments...")
      }
    }
  }
}
