//
//  ProductData.swift
//  ADAgraph
//
//  Created by Joel Oksanen on 13.6.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import Foundation

struct ProductData: Decodable {
  
  let root: Argument
  let info: ProductInfo
  
}
