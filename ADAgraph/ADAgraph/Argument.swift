//
//  Argument.swift
//  ADAgraph
//
//  Created by Joel Oksanen on 13.6.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import Foundation
import SwiftUI

struct Argument: Decodable, Equatable {
  
  let text: String
  let polarity: Polarity
  let supporters: [Argument]
  let attackers: [Argument]
  let phrase: String
  let size: CGFloat
  
  static func == (lhs: Argument, rhs: Argument) -> Bool {
    return lhs.text == rhs.text
  }
  
  func children() -> [Argument] {
    return supporters + attackers
  }
  
  func hasChild() -> Bool {
    return children().count > 0
  }
  
  func color() -> Color {
    return polarity == .POS ? Color(red: 121/255, green: 197/255, blue: 88/255) : Color(red: 220/255, green: 86/255, blue: 72/255)
  }
  
  func strokeColor() -> Color {
    return polarity == .POS ? Color(red: 90/255, green: 150/255, blue: 60/255) : Color(red: 180/255, green: 65/255, blue: 56/255)
  }
  
}

enum Polarity: String, Decodable {
  
  case POS
  case NEG
  
}
