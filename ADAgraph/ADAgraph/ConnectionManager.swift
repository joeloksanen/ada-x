//
//  ConnectionManager.swift
//  ADAgraph
//
//  Created by Joel Oksanen on 13.6.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

class ConnectionManager: ObservableObject {
  @Published var graph: Graph!
  @Published var product: Product!
  
  private let ip = "192.168.1.104"
  private let port = "8000"
  
  init() {
    requestArguments(id: "B0000TIKK8")
  }
  
  private func requestArguments(id: String) {
    let url = URL(string: "http://" + ip + ":" + port + "/ios_server/arguments?id=" + id)!
    
    let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
      guard let data = data else { return }
      do {
        let resp = try JSONDecoder().decode(ProductData.self, from: data)
        DispatchQueue.main.async {
          self.graph = Graph.fromRoot(resp.root)
          print(resp)
          self.requestImage(for: resp.info)
        }
      } catch let parseError {
        print(parseError)
        DispatchQueue.main.async {
          // Handle error in UI
        }
      }
    }
    task.resume()
  }
  
  private func requestImage(for productInfo: ProductInfo) {
    let url = URL(string: productInfo.imageURL)!
    
    let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
      guard let data = data else { return }
      if let image = UIImage(data: data) {
          DispatchQueue.main.async {
            self.product = Product(id: productInfo.id, name: productInfo.name, starRating: productInfo.starRating, image: image)
          }
      }
    }
    task.resume()
  }

}
