//
//  ProductInfo.swift
//  ADAbot
//
//  Created by Joel Oksanen on 23.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

struct ProductInfo: Decodable {
  
  let id: String
  let name: String
  let starRating: Double
  let imageURL: String
  
  init() {
    self.id = ""
    self.name = ""
    self.starRating = 0
    self.imageURL = ""
  }
  
}
