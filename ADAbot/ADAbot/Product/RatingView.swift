//
//  StarView.swift
//  ADAbot
//
//  Created by Joel Oksanen on 27.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct RatingView: View {
  let dim: CGFloat = 16
  var starRating: Double
  var fullStars: Int {
    get {
      return Int(starRating.rounded(.down))
    }
  }
  var starFrac: CGFloat {
    get {
      let frac = CGFloat(starRating - starRating.rounded(.down))
      return frac >= 0.25 ? (frac <= 0.75 ? frac : 0.75) : 0.25
    }
  }
  var emptyStars: Int {
    get {
      let n = 4 - fullStars
      return n > 0 ? n : 0
    }
  }
  
  var body: some View {
    HStack(spacing: 1) {
      ForEach(0..<fullStars, id: \.self) { i in
        ZStack {
          Image("star_fill")
            .renderingMode(.template)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .foregroundColor(Color(red: 254/255, green: 200/255, blue: 0/255))
          Image("star_border")
            .renderingMode(.template)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .foregroundColor(Color(red: 176/255, green: 131/255, blue: 58/255))
        }
        .frame(width: self.dim, height: self.dim)
      }
      
      if starFrac != 0 {
        ZStack(alignment: .leading) {
          Image("star_fill")
            .renderingMode(.template)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: self.dim * starFrac, height: self.dim, alignment: .leading)
            .foregroundColor(Color(red: 254/255, green: 200/255, blue: 0/255))
            .clipped()
          Image("star_border")
            .renderingMode(.template)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .foregroundColor(Color(red: 176/255, green: 131/255, blue: 58/255))
            .frame(width: self.dim, height: self.dim)
        }
      }
      
      ForEach(0..<emptyStars, id: \.self) { i in
        Image("star_border")
          .renderingMode(.template)
          .resizable()
          .aspectRatio(contentMode: .fill)
          .foregroundColor(Color(red: 176/255, green: 131/255, blue: 58/255))
          .frame(width: self.dim, height: self.dim)
      }
      
      Spacer()
    }
  }
}

