//
//  ProductView.swift
//  ADAbot
//
//  Created by Joel Oksanen on 23.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct ProductView: View {
  @ObservedObject var connectionManager: ConnectionManager
  let height: CGFloat = 200
  
  var body: some View {
    ZStack {
      Rectangle()
        .foregroundColor(Color.white)
        .frame(height: height)
        .shadow(color: Color(.sRGB, white: 0, opacity: 0.1), radius: 10, x: 0, y: 0)
      
      VStack {
        Spacer()
        HStack(alignment: .top) {
          VStack(alignment: .leading) {
            Text(connectionManager.product.name)
              .font(Font.custom("Helvetica Neue", size: 14))
              .foregroundColor(Color.gray)
            RatingView(starRating: connectionManager.product.starRating)
          }
          Spacer()
          Image(uiImage: connectionManager.product.image)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 120, height: 120)
            .border(Color(white: 0.85), width: 1)
        }
        .padding(EdgeInsets(top: 0, leading: 20, bottom: 20, trailing: 20))
      }
      .frame(height: height)
      
    }
  }
  
}
