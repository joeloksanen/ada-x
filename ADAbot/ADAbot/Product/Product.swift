//
//  Product.swift
//  ADAbot
//
//  Created by Joel Oksanen on 23.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import UIKit

struct Product: Identifiable {
  
  var id: String
  var name: String
  var starRating: Double
  var image: UIImage
  
}
