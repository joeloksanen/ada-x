//
//  ConnectionManager.swift
//  ADAbot
//
//  Created by Joel Oksanen on 23.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

class ConnectionManager: ObservableObject {
  @Published var products = [Product]()
  @Published var product: Product!
  @Published var messaging: Bool = false
  @Published var messages: [Message] = [Message]()
  
  private let ip = "192.168.1.104"
  private let port = "8000"
  private var messageQueue: [Message]? = nil
  private var productMap = [String: Product]()
  
  init() {
    requestProducts()
    // B00RTGK0N0 - red Canon camera
    // B004J3V90Y - Canon T3i
    // B0012YA85A - Canon Rebel XSI
    // B003ZYF3LO - Nikon D3100
    // B0075SUK14 - Backpack
    // B000AYW0M2 - Watch
    // B000ZKA0J6 - Starcraft game
    // B00005UP2N - Silver Mixer
    // B0001HLTTI - Another Mixer
  }
  
  private func requestProducts() {
    let url = URL(string: "http://" + ip + ":" + port + "/ios_server/products")!
    
    let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
      guard let data = data else { return }
      do {
        let resp = try JSONDecoder().decode([ProductInfo].self, from: data)
        DispatchQueue.main.async {
          for productInfo in resp {
            self.requestImage(for: productInfo)
          }
        }
      } catch let parseError {
        print(parseError)
        DispatchQueue.main.async {
          // Handle error in UI
        }
      }
    }
    task.resume()
  }
  
  private func requestImage(for productInfo: ProductInfo) {
    let url = URL(string: productInfo.imageURL)!
    
    let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
      guard let data = data else { return }
      if let image = UIImage(data: data) {
          DispatchQueue.main.async {
            let product = Product(id: productInfo.id, name: productInfo.name, starRating: productInfo.starRating, image: image)
            self.productMap[productInfo.id] = product
            self.products.append(product)
          }
      }
    }
    task.resume()
  }
  
  func requestProduct(id: String) {
    let url = URL(string: "http://" + ip + ":" + port + "/ios_server/product?id=" + id)!
    
    let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
      guard let data = data else { return }
      do {
        let resp = try JSONDecoder().decode(ADAMessage.self, from: data)
        DispatchQueue.main.async {
          self.addMessage(resp)
          self.product = self.productMap[id]
          self.messaging = true
        }
      } catch let parseError {
        print(parseError)
        DispatchQueue.main.async {
          // Handle error in UI
        }
      }
    }
    task.resume()
  }
  
  func sendQuery(_ query: ArgumentQuery) {
    let url = URL(string: "http://" + ip + ":" + port + "/ios_server/message/")!
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    guard let data = try? JSONEncoder().encode(query) else {
      return
    }
    
    let task = URLSession.shared.uploadTask(with: request, from: data) { receivedData, response, error in
      if let error = error {
        self.handleClientError(error: error)
        return
      }
      guard let httpResponse = response as? HTTPURLResponse,
        (200...299).contains(httpResponse.statusCode) else {
          self.handleServerError(response: response!)
          return
      }
      if let mimeType = httpResponse.mimeType,
        mimeType == "application/json",
        let receivedData = receivedData {
        do {
          print(receivedData)
          let resp = try JSONDecoder().decode(ADAMessage.self, from: receivedData)
          DispatchQueue.main.async {
            self.addMessage(resp)
          }
        } catch let parseError {
          print(parseError)
          DispatchQueue.main.async {
            // Handle error in UI
          }
        }
      }
    }
    task.resume()
  }
  
  func addMessage(_ message: Message) {
    if messageQueue != nil {
      messageQueue!.append(message)
    } else {
      messages.append(message)
    }
  }
  
  func addMessageWithDelay(_ message: Message, delay: Double) {
    messageQueue = [Message]()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
      self.messages.append(message)
      for message in self.messageQueue! {
        withAnimation(.linear(duration: 0.3)) {
          self.messages.append(message)
        }
      }
      self.messageQueue = nil
    }
  }
  
  func quitMessaging() {
    self.messaging = false
    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
      self.messages = [Message]()
      self.product = nil
    }
  }
  
  private func handleClientError(error: Error) {
    print("Client error occurred")
  }
  private func handleServerError(response: URLResponse) {
    print("Server error occurred")
  }
  
}
