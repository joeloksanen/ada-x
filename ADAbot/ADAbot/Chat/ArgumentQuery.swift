//
//  FeatureResponse.swift
//  ADAbot
//
//  Created by Joel Oksanen on 24.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import UIKit

struct ArgumentQuery: Codable {
  
  let id = UUID()
  let argumentID: Int
  let queryID: Int
  let text: String
  var message: UserMessage {
    get {
      return UserMessage(query: self)
    }
  }
  
}
