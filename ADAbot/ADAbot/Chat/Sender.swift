//
//  Sender.swift
//  ADAbot
//
//  Created by Joel Oksanen on 24.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

enum Sender: String, Decodable {
  case ADA
  case USER
}
