//
//  Feature.swift
//  ADAbot
//
//  Created by Joel Oksanen on 24.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import UIKit

struct Argument: Codable, Equatable {
  
  let id = UUID()
  let name: String
  let queries: [ArgumentQuery]
  
  static func == (lhs: Argument, rhs: Argument) -> Bool {
    lhs.id == rhs.id
  }
  
}
