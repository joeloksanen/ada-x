//
//  FeatureBubble.swift
//  ADAbot
//
//  Created by Joel Oksanen on 24.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct FeatureView: View {
  @ObservedObject var chatManager: ChatManager

  static let bubbleColor = Color(red: 75/255, green: 90/255, blue: 116/255)
  let arguments: [Argument]
  @State var showingQueryOptionsForArgument: Argument?
  
  var body: some View {
    VStack(spacing: 0) {
      if (!chatManager.sent) {
        HStack(spacing: 0) {
          Spacer()
          
          Text("ASK ABOUT")
            .foregroundColor(FeatureView.bubbleColor)
            .font(Font.custom("Gill Sans", size: 11))
            .fixedSize(horizontal: true, vertical: false)
            .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
            
          Capsule()
            .foregroundColor(FeatureView.bubbleColor)
            .frame(width: 1, height: 38)
          
          ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 0) {
              Spacer()
                .frame(width: 10)
              ForEach(self.arguments, id: \.id) { argument in
                FeatureBubble(argument: argument,
                              showingQueryOptionsForArgument: self.$showingQueryOptionsForArgument)
                  .onTapGesture {
                    withAnimation(.easeInOut(duration: 0.3)) {
                      self.showingQueryOptionsForArgument = argument
                    }
                  }
              }
              Spacer()
                .frame(width: 10)
            }
          }
          .frame(maxWidth: 280)
          .fixedSize(horizontal: true, vertical: false)
        }
        .padding(EdgeInsets(top: 0, leading: 0, bottom: 15, trailing: 0))
      }
      
      if showingQueryOptionsForArgument != nil {
        QueryOptionView(chatManager: chatManager, queries: showingQueryOptionsForArgument!.queries)
      }
    }
    .frame(width: UIScreen.main.bounds.width)
  }
}

struct FeatureBubble: View {
  let argument: Argument
  @Binding var showingQueryOptionsForArgument: Argument?
  
  var body: some View {
    Text(argument.name)
      .foregroundColor(self.showingQueryOptionsForArgument == argument ? Color.white : FeatureView.bubbleColor)
      .font(Font.custom("Helvetica Neue", size: 12))
      .fixedSize(horizontal: true, vertical: true)
      .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
      .background(self.showingQueryOptionsForArgument == argument ?
        AnyView(Capsule().foregroundColor(FeatureView.bubbleColor)) :
        AnyView(Capsule().strokeBorder(FeatureView.bubbleColor))
      )
      .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10))
  }
}
