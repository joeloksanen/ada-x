//
//  ADAText.swift
//  ADAbot
//
//  Created by Joel Oksanen on 12.6.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import UIKit

struct ArgumentText: Decodable {
  
  let template: String
  let arguments: [String]
  let style: TextStyle
  
}

enum TextStyle: String, Decodable {
  
  case ARG
  case QUOT
  
}
