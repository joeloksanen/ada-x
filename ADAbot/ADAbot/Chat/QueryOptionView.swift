//
//  QueryOptionView.swift
//  ADAbot
//
//  Created by Joel Oksanen on 25.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct QueryOptionView: View {
  @ObservedObject var chatManager: ChatManager
  let queries: [ArgumentQuery]
  
  var body: some View {
    VStack(spacing: 0) {
      ForEach(queries, id: \.id) { query in
        MessageView(chatManager: self.chatManager, query: query)
      }
    }
  }
}

