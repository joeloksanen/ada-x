//
//  ChatView.swift
//  ADAbot
//
//  Created by Joel Oksanen on 23.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct ChatView: View {
  @ObservedObject var connectionManager: ConnectionManager
  @ObservedObject var chatManager: ChatManager
  
  init(connectionManager: ConnectionManager) {
    self.connectionManager = connectionManager
    self.chatManager = ChatManager(connectionManager: connectionManager)
  }
  
  var body: some View {
    ZStack {
      Rectangle()
        .foregroundColor(Color(red: 231/255, green: 234/255, blue: 239/255))
      
      if !connectionManager.messages.isEmpty {
        ScrollView {
          VStack(spacing: 0) {
            ForEach(connectionManager.messages, id: \.id) { message in
              MessageView(chatManager: self.chatManager, message: message)
            }
          }
          .padding(EdgeInsets(top: 30, leading: 0, bottom: 30, trailing: 0))
        }
      }
    }
  }
}
