//
//  UserMessage.swift
//  ADAbot
//
//  Created by Joel Oksanen on 24.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import UIKit

struct UserMessage: Message, Equatable {

  let id = UUID()
  let text: String
  let arguments = [Argument]()
  let sent: Bool
  let sender: Sender = .USER
  
  init(text: String) {
    self.text = text
    self.sent = true
  }
  
  init(query: ArgumentQuery) {
    self.text = query.text
    self.sent = false
  }
  
  static func == (lhs: UserMessage, rhs: UserMessage) -> Bool {
    lhs.id == rhs.id
  }
  
  func getAttributedText() -> NSAttributedString {
    let attributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 14)!,
                      NSAttributedString.Key.foregroundColor: UIColor.white]
    return NSAttributedString(string: text, attributes: attributes)
  }
  
}
