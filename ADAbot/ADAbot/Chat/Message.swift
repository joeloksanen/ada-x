//
//  Message.swift
//  ADAbot
//
//  Created by Joel Oksanen on 24.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import UIKit

protocol Message {

  var id: UUID { get }
  var sender: Sender { get }
  var arguments: [Argument] { get }
  func getAttributedText() -> NSAttributedString
  
}


