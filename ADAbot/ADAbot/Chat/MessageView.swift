//
//  MessageView.swift
//  ADAbot
//
//  Created by Joel Oksanen on 24.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct MessageView: View {
  @ObservedObject var chatManager: ChatManager
  
  let bubbleColors = [
    Sender.ADA: Color(red: 242/255, green: 159/255, blue: 31/255),
    Sender.USER: Color(red: 35/255, green: 45/255, blue: 62/255)
  ]
  let maxWidth: CGFloat = 260
  
  let message: Message
  let query: ArgumentQuery?
  @State var sent: Bool
  
  init(chatManager: ChatManager, message: Message) {
    self.chatManager = chatManager
    self.message = message
    self.query = nil
    _sent = State(initialValue: true)
  }
  
  init(chatManager: ChatManager, query: ArgumentQuery) {
    self.chatManager = chatManager
    self.message = query.message
    self.query = query
    _sent = State(initialValue: false)
  }
  
  var body: some View {
    VStack(spacing: 0) {
      if !chatManager.sent || self.sent {
        HStack(spacing: 0) {
          if message.sender == .USER {
            Spacer()
          }
          
          Label(maxWidth: maxWidth, attributedText: self.message.getAttributedText())
            .foregroundColor(Color.white)
            .fixedSize(horizontal: true, vertical: true)
            .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
            .background(MessageBubble(sender: message.sender).foregroundColor(sent ? bubbleColors[message.sender] : FeatureView.bubbleColor))
            .onTapGesture {
              withAnimation(.easeInOut(duration: 0.3)) {
                if self.message.sender == .ADA {
                  self.chatManager.showOptionsFor(self.message as! ADAMessage)
                } else if !self.sent {
                  self.chatManager.sendQuery(query: self.query!)
                  self.sent = true
                }
              }
            }
          
          if message.sender == .ADA {
            Spacer()
          }
        }
        .padding(EdgeInsets(top: 0, leading: 20, bottom: 15, trailing: 20))
      }
      
      if chatManager.showingOptionsFor(message) {
        FeatureView(chatManager: chatManager, arguments: message.arguments)
      }
    }
    .frame(width: UIScreen.main.bounds.width)
  }
}

struct Label: UIViewRepresentable {
  
  var maxWidth: CGFloat
  var attributedText: NSAttributedString

  func makeUIView(context: Context) -> UILabel {
      let label = UILabel()
      label.lineBreakMode = .byWordWrapping
      label.attributedText = attributedText
      label.numberOfLines = 0
      label.preferredMaxLayoutWidth = maxWidth
      return label
  }

  func updateUIView(_ view: UILabel, context: Context) {
  }
}
