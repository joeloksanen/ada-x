//
//  ChatManager.swift
//  ADAbot
//
//  Created by Joel Oksanen on 25.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

class ChatManager: ObservableObject {
  @Published var sent = false
  @Published var showingOptionsForMessage: ADAMessage?
  
  let connectionManager: ConnectionManager
  
  init(connectionManager: ConnectionManager) {
    self.connectionManager = connectionManager
  }
  
  func sendQuery(query: ArgumentQuery) {
    connectionManager.sendQuery(query) 
    sent = true
    connectionManager.addMessageWithDelay(query.message, delay: 0.3)
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
      self.sent = false
      self.clearOptions()
    }
  }
  
  func showOptionsFor(_ message: ADAMessage) {
    showingOptionsForMessage = showingOptionsForMessage == message ? nil : message
  }
  
  func showingOptionsFor(_ message: Message) -> Bool {
    return message is ADAMessage && (message as! ADAMessage) == showingOptionsForMessage
  }
  
  func clearOptions() {
    self.showingOptionsForMessage = nil
  }
  
}

