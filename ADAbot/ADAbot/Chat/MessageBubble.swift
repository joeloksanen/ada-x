//
//  MessageBubble.swift
//  ADAbot
//
//  Created by Joel Oksanen on 24.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI
import UIKit

struct MessageBubble: Shape {

  let cornerRadius: CGFloat = 10
  let sender: Sender

  func path(in rect: CGRect) -> Path {
    let path = UIBezierPath(roundedRect: rect, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
    
    let curveCenter = CGPoint(x: rect.width - cornerRadius, y: rect.height - cornerRadius)
    var tailPath = UIBezierPath()
    tailPath.move(to: curveCenter)
    tailPath.addLine(to: CGPoint(x: curveCenter.x, y: rect.height))
    tailPath.addQuadCurve(to: CGPoint(x: rect.width + cornerRadius / 2, y: curveCenter.y),
                          controlPoint: CGPoint(x: rect.width + cornerRadius / 2, y: rect.height))
    tailPath.close()
    
    if (sender == .ADA) {
      tailPath.apply(CGAffineTransform(scaleX: -1, y: 1))
      tailPath.apply(CGAffineTransform(translationX: rect.width, y: 0))
    } else {
      tailPath = tailPath.reversing()
    }
    
    path.append(tailPath)
    return Path(path.cgPath)
  }
  
}
