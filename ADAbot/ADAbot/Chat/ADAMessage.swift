//
//  ADAMessage.swift
//  ADAbot
//
//  Created by Joel Oksanen on 24.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import UIKit

struct ADAMessage: Message, Decodable, Equatable {
  
  let id = UUID()
  let text: ArgumentText
  let arguments: [Argument]
  let sender: Sender = .ADA
  
  static func == (lhs: ADAMessage, rhs: ADAMessage) -> Bool {
    lhs.id == rhs.id
  }
  
  func getAttributedText() -> NSAttributedString {
    let components = text.template.components(separatedBy: "*")
    var argumentLocations = [(Int,Int)]()
    var textWithArguments = ""
    for (i, component) in components.enumerated() {
      textWithArguments += component
      if i < components.count - 1 {
        argumentLocations.append((textWithArguments.count, text.arguments[i].count))
        textWithArguments += text.arguments[i]
      }
    }
    let attributes = [NSAttributedString.Key.font: UIFont(name: text.style == .QUOT ? "HelveticaNeue-Italic" : "Helvetica Neue", size: 14)!,
                      NSAttributedString.Key.foregroundColor: UIColor.white]
    let attributedString = NSMutableAttributedString(string: textWithArguments, attributes: attributes)
    for (start, len) in argumentLocations {
      attributedString.addAttribute(NSAttributedString.Key.font,
                                    value: UIFont(name: text.style == .QUOT ? "HelveticaNeue-BoldItalic" : "HelveticaNeue-Bold", size: 14)!,
                                    range: NSMakeRange(start, len))
//      attributedString.addAttribute(NSAttributedString.Key.foregroundColor,
//                                    value: UIColor(red: 140/255, green: 100/255, blue: 15/255, alpha:1.0).cgColor,
//      range: NSMakeRange(start, len))
    }
    return attributedString
  }
  
}
