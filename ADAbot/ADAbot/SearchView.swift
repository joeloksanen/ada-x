//
//  SearchView.swift
//  ADAbot
//
//  Created by Joel Oksanen on 12.6.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct SearchView: View {
  @ObservedObject var connectionManager: ConnectionManager
  let height: CGFloat = 120
  
  var body: some View {
    VStack(spacing: 0) {
      ZStack {
        Rectangle()
        .foregroundColor(Color.white)
        .frame(height: height)
        .shadow(color: Color(.sRGB, white: 0, opacity: 0.1), radius: 10, x: 0, y: 0)
        VStack {
          Spacer()
          .frame(height: 30)
          HStack {
            Text("Products")
            .font(Font.custom("HelveticaNeue-Bold", size: 28))
              .foregroundColor(Color.gray)
              .padding(EdgeInsets(top: 0, leading: 30, bottom: 0, trailing: 0))
            Spacer()
        }
        }
      }
      .zIndex(10)
      
      List(connectionManager.products) { product in
        ProductRow(product: product)
          .onTapGesture {
            self.connectionManager.requestProduct(id: product.id)
          }
      }
    }
    .edgesIgnoringSafeArea(.all)
  }
}

struct ProductRow: View {
  var product: Product
  
  var body: some View {
    HStack(spacing: 0) {
      Image(uiImage: product.image)
        .resizable()
        .aspectRatio(contentMode: .fit)
        .frame(width: 100, height: 100)
        .border(Color(white: 0.85), width: 1)
        .padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 20))
      VStack {
        Text(product.name)
          .font(Font.custom("Helvetica Neue", size: 14))
          .foregroundColor(Color.gray)
        RatingView(starRating: product.starRating)
      }
    }
  }
}
