//
//  InitResponse.swift
//  ADAbot
//
//  Created by Joel Oksanen on 27.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

struct InitResponse: Decodable {
  
  let productInfo: ProductInfo
  let message: ADAMessage
  
}
