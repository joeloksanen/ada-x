//
//  ProductView.swift
//  ADAbot
//
//  Created by Joel Oksanen on 12.6.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct ADAView: View {
  @ObservedObject var connectionManager: ConnectionManager
  
  var drag: some Gesture {
      DragGesture()
        .onEnded { _ in self.connectionManager.quitMessaging() }
  }
  
  var body: some View {
    VStack(spacing: 0) {
      ProductView(connectionManager: connectionManager)
        .zIndex(10)
        .gesture(drag)
      ChatView(connectionManager: connectionManager)
        .zIndex(0)
    }
    .edgesIgnoringSafeArea(.all)
    .background(Color.black)
  }
}
