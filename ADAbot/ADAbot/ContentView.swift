//
//  ContentView.swift
//  ADAbot
//
//  Created by Joel Oksanen on 23.2.2020.
//  Copyright © 2020 Joel Oksanen. All rights reserved.
//

import SwiftUI

struct ContentView: View {
  @ObservedObject var connectionManager = ConnectionManager()
  @State private var searching: Bool = true
  
  var body: some View {
    VStack(spacing: 0) {
      if !connectionManager.messaging {
        SearchView(connectionManager: connectionManager)
      } else {
        ADAView(connectionManager: connectionManager)
      }
    }
    .background(Color.black)
  }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
