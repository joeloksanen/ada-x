# ADA-X

ADA-X is a system for domain-independent feature-based product review aggregation.
Click [here](https://www.youtube.com/watch?v=-XoKmp-yt3M) for a brief video demonstration of the project.

## Server

The server-side implementation of ADA-X can be found in **/ADA-X/server/agent**

- **/agent** contains the ADA code
- **/target_extraction** contains the ontology extraction code
- **/SA** contains the sentiment analysis code


## ADAbot

**/ADAbot** contains the conversational front-end code

## ADAgraph

**/ADAgraph** contains the graph-based front-end code
